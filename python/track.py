#!/usr/bin/python

import math
import tune
import json
import vector
import colourscale

class TrackSection:

	def __init__( self, data, pos, tang_i, dist_i, laneoffset ) :
		self.dist_i = dist_i
		self.tang_i = tang_i
		self.sw = False
		self.x = vector.Vec2d(1.0,0.0).rotated( self.tang_i )
		self.y = vector.Vec2d(0.0,1.0).rotated( self.tang_i )
		self.pos_i = pos

		if "length" in data :
			self.length = data[ "length" ]
			self.radius = 0.0
			self.angle = 0.0
			self.tang_o = tang_i
			self.tang_d = 0.0
			self.pos_o = self.pos_i + self.length * self.y

		if "angle" in data :
			self.angle = math.radians( data[ "angle" ] )
			self.radius = data[ "radius" ]
			self.tang_o = tang_i + self.angle
			if self.tang_o > math.pi:
				self.tang_o -= 2*math.pi
			if self.tang_o < -math.pi:
				self.tang_o += 2*math.pi
			# Adjust for lane offset
			if self.angle < 0 :
				self.radius += laneoffset
			else :
				self.radius -= laneoffset
			# Calculate section length
			self.length = abs(self.angle) * self.radius
			self.tang_d = self.angle / self.length
			# Calculate end point
			if self.angle < 0 :
				frc = -self.radius * self.x
			else:
				frc =  self.radius * self.x
			toend = frc.rotated( self.angle )
			cx = self.pos_i - frc
			self.pos_o = cx + toend
			#print "self.pos_i", self.pos_i, "cx", cx, "frc", frc, "toend", toend, "pos_o", self.pos_o

		if "switch" in data and data["switch"]==True:
			self.sw = True

		self.dist_o = self.dist_i + self.length

	def __repr__( self ) :
		return "TrackSection with angle=%f,radius=%f,length=%f,dist_i=%f,tang_d=%f,sw=%d" % ( self.angle, self.radius, self.length, self.dist_i, self.tang_d, self.sw )


	def as_svg( self ) :
		return '<line x1="%f" y1="%f" x2="%f" y2="%f" style="stroke:rgb(255,0,0);stroke-width:10" />' % ( -self.pos_i[0], -self.pos_i[1], -self.pos_o[0], -self.pos_o[1] )

	def tangent_at_pos( self, p ) :
		t = ( p - self.dist_i ) / self.length
		tangent = self.tang_i + t * self.angle
		if tangent < -math.pi :
			tangent += 2*math.pi
		if tangent > math.pi:
			tangent -= 2*math.pi
		return tangent

	def coords_at_pos( self, p ) :
		t = ( p - self.dist_i ) / self.length
		a = t * self.angle
		if self.angle == 0 :
			return self.pos_i + t * self.length * self.y
		if self.angle < 0 :
			frc = -self.radius * self.x
		else :
			frc =  self.radius * self.x
		toend = frc.rotated( a )
		cx = self.pos_i - frc
		return cx + toend

class Track:

	def __init__( self, data, laneoffset ) :

		pieces = data[ "pieces" ]
		angle0 = math.radians( data[ "startingPoint" ][ "angle" ] )
		self.sections = []
		angle = angle0
		dist  = 0.0
		pos = vector.Vec2d( 0.0, 0.0 )
		pos += vector.Vec2d( -laneoffset, 0.0 ).rotated( angle0 )
		for nr, piece in enumerate(pieces) :
			s = TrackSection( piece, pos, angle, dist, laneoffset )
			#print nr, s
			self.sections.append( s )
			angle = s.tang_o
			dist += s.length
			pos = s.pos_o
		self.sample( 2.0 )

	def as_svg_lores( self ) :
		rv = "<svg>\n"
		for s in self.sections:
			rv += s.as_svg()+"\n"
		rv += "</svg>\n"
		return rv

	def as_svg( self, cs ) :
		rv = "<svg>\n"
		for i in range( self.numsamples ) :
			c0 = self.coords[ i+0 ]
			c1 = self.coords[ i+1 ]
			bu_l = self.buildup_l[i]
			bu_r = self.buildup_r[i]
			bu = max( abs(bu_l), abs(bu_r) )
			rgb = cs.map( bu )
			rv += '<line x1="%f" y1="%f" x2="%f" y2="%f" style="stroke:rgb(%f,%f,%f);stroke-width:10" />\n' % ( -c0[0], -c0[1], -c1[0], -c1[1], rgb[0], rgb[1], rgb[2] )
		rv += "</svg>\n"
		return rv

	def length( self ) :
		l = 0.0
		for s in self.sections :
			l += s.length
		return l

	def sample( self, res, buildups=None ) :
		self.res = res
		self.samples_a  = []
		self.samples_da = []
		self.buildup_r = []
		self.buildup_l = []
		self.coords = []
		self.numsamples = 0
		sidx = 0
		dist = 0.0
		bu_l = 0.0
		bu_r = 0.0
		if buildups:
			bu_l = buildups[0]
			bu_r = buildups[1]
		l = self.length()
		while dist < l :
			s = self.sections[ sidx ]
			if s.dist_o < dist :
				sidx += 1
				s = self.sections[ sidx ]
			assert sidx < len( self.sections )
			s = self.sections[ sidx ]
			coord = s.coords_at_pos( dist )
			self.coords.append( coord )
			tangent = s.tangent_at_pos( dist )
			self.samples_a.append( tangent )
			self.samples_da.append( s.tang_d )
			if s.tang_d > 0:
				bu_r = bu_r + s.tang_d
				#bu_l = tune.fastdecay * bu_l
			if s.tang_d < 0:
				bu_l = bu_l + s.tang_d
				#bu_r = tune.fastdecay * bu_r
			#if s.tang_d == 0:
			#	bu_l = tune.fastdecay * bu_l
			#	bu_r = tune.fastdecay * bu_r
			bu_l = tune.decay * bu_l # was 0.95
			bu_r = tune.decay * bu_r
			self.buildup_r.append( bu_r )
			self.buildup_l.append( bu_l )
			dist += res
			self.numsamples += 1

		if buildups == None:
			self.sample( res, [ bu_l, bu_r ] )
		else:
			self.samples_a = self.samples_a + self.samples_a
			self.samples_da = self.samples_da + self.samples_da
			self.buildup_l = self.buildup_l + self.buildup_l
			self.buildup_r = self.buildup_r + self.buildup_r
			self.coords = self.coords + self.coords

	def buildups_at_abspos( self, p ):
		idx = int( round ( p / self.res ) )
		if idx >= len( self.buildup_l ) or idx >= len( self.buildup_r ) :
			print "abspos %d has index %d out for range.%d/%d" % ( p, idx, len(self.buildup_l), len(self.buildup_r) )
		return ( self.buildup_l[idx], self.buildup_r[idx] )

	def buildups_at_range( self, p0, p1 ) :
		idx0 = int( round ( p0 / self.res ) )
		idx1 = int( round ( p1 / self.res ) )
		minl = 0
		maxr = 0
		avgl = 0
		avgr = 0
		cnt = 0
		for idx in range( idx0, idx1 ) :
			bu_l = self.buildup_l[idx]
			bu_r = self.buildup_r[idx]
			minl = min( bu_l, minl )
			maxr = max( bu_r, maxr )
			avgl += bu_l
			avgr += bu_r
			cnt += 1
		if cnt==0:
			cnt=1
		avgl = avgl / cnt
		avgr = avgr / cnt
		return ( (avgl,avgr), (minl,maxr) )

	def buildups_over_range( self, p0, p1 ) :
		idx0 = int( round ( p0 / self.res ) )
		idx1 = int( round ( p1 / self.res ) )
		if idx0 == idx1:
			idx1 += 1
		buildups_l = []
		buildups_r = []
		for idx in range( idx0, idx1 ) :
			t = float(idx-idx0) / float(idx1-idx0)
			u = 1.0 - t
			bu_l = self.buildup_l[idx] * u
			bu_r = self.buildup_r[idx] * u
			buildups_l.append( bu_l )
			buildups_r.append( bu_r )
		return ( min(buildups_l), max(buildups_r) )

	def print_tangents( self ) :
		sz = len(self.samples_a)
		for i in range(sz):
			a = self.samples_a[i]
			da = self.samples_da[i]
			bu_l = self.buildup_l[i]
			bu_r = self.buildup_r[i]
			print "a=%f da=%f bu_l=%f bu_r=%f" % ( a, da, bu_l, bu_r )

	def __repr__( self ) :
		return "Track with %d sections and total length %f" % ( len( self.sections ), self.length() )


if __name__ == "__main__":

	txt = open( "gi_finland.txt", "r" ).read()
	o = json.loads( txt )

	race = o[ "data" ][ "race" ]

	tracks = []
	for lane in race[ "track" ][ "lanes" ] :
		laneoffset = lane[ "distanceFromCenter" ]
		t = Track( race[ "track" ], laneoffset )
		tracks.append(t)

	mv = 0
	for t in tracks :
		mv = max( mv, abs(min(t.buildup_l)), abs(max(t.buildup_r)))
	cs = colourscale.ColourScale()
	cs.set_mark( 0.00*mv, (0,0,255) )
	cs.set_mark( 0.25*mv, (0,255,255) )
	cs.set_mark( 0.50*mv, (0,255,0) )
	cs.set_mark( 0.75*mv, (255,255,0) )
	cs.set_mark( 1.00*mv, (255,0,0) )
	for t in tracks:
		print t.as_svg( cs )


