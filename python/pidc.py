#!/usr/bin/python


import math

import track

class PIDC:

	def __init__( self, kp, ki, kd ) :
		"""
		Create a PID controller.
		Arguments:
			kp -- Proportional constant
			ki -- Integral constant
			kd -- Differential constant
		Returns:
			newly created PID controller object
		"""
		self.P = kp
		self.I = ki
		self.D = kd
		self.previousError = 0
		self.integralError = 0
		self.fresh = True
		self.angular = False

	def reset( self ) :
		"""
		Reset the PID controller. Historic error and previous error are cleared.
		"""
		self.previousError = 0
		self.integralError = 0
		self.fresh = True

	def update( self, dt, ist, soll ) :
		"""
		Update the PID controller and calculate a steering value.
		Arguments:
			dt -- delta time in seconds
			ist -- the current value
			soll -- the desired value
		Returns:
			steering value
		"""

		if dt <= 0.0 :
			 return 0.0;

		error = ist - soll;

		if self.angular :
			# normalize angular error
			if ( error < -math.pi ) :
				error += 2*math.pi
			if ( error >  math.pi ) :
				error -= 2*math.pi

		if self.fresh :
			self.integralError = error
			self.previousError = error
	
		self.integralError = ( 1.0 - dt ) * self.integralError +  dt * error;
		derivativeError = ( error - self.previousError ) / dt;
		self.previousError = error;
		self.fresh = False;
		return self.P * error + self.I * self.integralError + self.D * derivativeError;

import json

if __name__ == "__main__":
	p = PIDC( -9, -1.0, -6 )
	ist = 10
	soll = 5
	v = 0
	dt = 1/60.0
	for i in range(2) :
		a = p.update( dt, ist, soll )
		print "ist",ist, "v",v, "a", a
		v += dt * a
		ist += dt * v

	txt = open( "gi.txt", "r" ).read()
	o = json.loads( txt )

	race = o[ "data" ][ "race" ]

	for lane in race[ "track" ][ "lanes" ] :
		laneoffset = lane[ "distanceFromCenter" ]
		t = track.Track( race[ "track" ], laneoffset )
		print t
		t.print_tangents()
		#bu_l, bu_r =  t.buildups_at_abspos( 800.0 )
		#print bu_l, bu_r





