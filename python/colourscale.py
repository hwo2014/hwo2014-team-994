
class ColourScale:

	def __init__( self ) :
		self.marks = []

	def set_mark( self, value, colour ) :
		self.marks.append( ( value, colour ) )
		self.marks.sort()

	def map( self, value ) :
		for i, b in enumerate( self.marks ) :
			if value < b[0] :
				if i==0 :
					return b[1]
				a = self.marks[ i-1 ]
				av = a[0]
				bv = b[0]
				d  = bv - av
				t  = ( value - av ) / d
				u  = 1.0 - t
				ac = a[1]
				bc = b[1]
				return ( u*ac[0] + t*bc[0], u*ac[1] + t*bc[1], u*ac[2] + t*bc[2] )
		return self.marks[-1][1]

