import json
import socket
import sys

import track
import pidc
import tune


class NoobBot(object):

	def __init__(self, socket, name, key):
		self.socket = socket
		self.name = name
		self.key = key
		self.tracks = []
		self.prvpos = 0.0
		self.tick = -1
		#self.pid = pidc.PIDC( -2, -0.4, -0.2 )
		self.pid = pidc.PIDC( -1, -0.0, -0.0 )

	def msg(self, msg_type, data):
		self.send(json.dumps({"msgType": msg_type, "data": data}))

	def send(self, msg):
		self.socket.send(msg + "\n")

	def join(self, trackName):
		#return self.msg("join", {"name": self.name, "key": self.key})
		return self.msg("joinRace", {"botId":{"name":self.name, "key":self.key}, "trackName":trackName, "carCount":1})

	def createrace(self, trackName):
		return self.msg("createRace", {"botId":{"name":self.name, "key":self.key}, "trackName":trackName, "password":"", "carCount":2})

	def switchlane(self, direction):
		return self.msg("switchLane", direction)

	def throttle(self, throttle):
		self.msg("throttle", throttle)

	def ping(self):
		self.msg("ping", {})

	def run(self):
		#self.join( "germany" )
		self.join( "keimola" )
		#self.createrace( "germany" )
		self.msg_loop()

	def on_join(self, data, gametick):
		print data
		print("Joined")
		self.ping()

	def on_join_race(self, data, gametick):
		print data
		print "Joined Race"
		self.ping

	def on_game_init( self, data, gametick ):
		race = data[ "race" ]
		for lane in race[ "track" ][ "lanes" ] :
			laneoffset = lane[ "distanceFromCenter" ]
			t = track.Track( race[ "track" ], laneoffset )
			self.tracks.append( t )
		self.switching = {}
		if race["track"]["id"] == "keimola" :
			#self.switching = { 2:[0,"Right"], 7:[0,"Left"], 17:[0,"Right"] }
			self.switching = { 12:[0,"Right"], 17:[0,"Left"] }
			#self.switching = { 2:[0,"Right"], 7:[0,"Left"], 17:[0,"Right"], 34:[0,"Left"] }
		if race["track"]["id"] == "germany" :
			#self.switchorder="-----RL"
			self.switchorder="LR-LR--"
			#self.switchorder="RL---RL"
			swnr=0
			for nr, section in enumerate(t.sections):
				if nr>0 and section.sw:
					dir = None
					if self.switchorder[swnr]=="L":
						dir="Left"
					if self.switchorder[swnr]=="R":
						dir="Right"
					if dir:
						self.switching[nr-1] = [0,dir]
					swnr += 1

	def on_game_start(self, data, gametick):
		print data
		print("Race started")
		#self.ping()
		self.throttle( 1.0 )

	def on_car_positions(self, data, gametick):
		throttlesetting = 1.00
		switchidx = -1
		for d in data:
			if d["id"]["name"] == "Silberpfeil":
				deltatick = gametick - self.tick
				if deltatick != 1:
					print "MISSED A TICK!"
					print "EXPECTED", self.tick+1, "GOT", gametick
				angl = d["angle"]
				pp   = d["piecePosition"]
				lap  = pp["lap"]
				pidx = pp["pieceIndex"]
				ipds = pp["inPieceDistance"]
				slan = pp["lane"]["startLaneIndex"]
				elan = pp["lane"]["endLaneIndex"]
				slanlen = self.tracks[slan].sections[pidx].length
				elanlen = self.tracks[elan].sections[pidx].length
				lane = slan
				if ipds < 0.25*(slanlen+elanlen):
					lane = elan
				section = self.tracks[lane].sections[pidx]
				abspos = section.dist_i + ipds
				deltapos = abspos - self.prvpos
				#print "section.dist_i", section.dist_i,"ipds",ipds,"abspos",abspos,"deltapos",deltapos
				if deltapos < 0 :
					deltapos = 6.0
				self.prvpos = abspos
				#bu_l, bu_r = self.tracks[lane].buildups_at_abspos( abspos+16.0*deltapos ) # was 16
				#(avgl,avgr), (minl,maxr) = self.tracks[lane].buildups_at_range( abspos+0*deltapos, abspos+18*deltapos )
				#bu_l = (avgl+minl)/2
				#bu_r = (avgr+maxr)/2
				#bu_l = avgl
				#bu_r = avgr
				#u = abs(bu_l) + abs(bu_r)
				#bu = max( abs(bu_l), abs(bu_r) )

				bu_l, bu_r = self.tracks[lane].buildups_over_range( abspos+tune.scanlo*deltapos, abspos+tune.scanhi*deltapos )
				bu = max( abs(bu_l), abs(bu_r) )

				# Emprically determined: at build up 0.17 the velocity should be appr 7.
				# Max vel seems to be roughly 10.
				#desiredvel = 10.4 - 15.6*bu # for Finland track.
				#desiredvel = 10.4 - 15.0*bu # was 15.0 for avg buildup
				desiredvel = tune.maxvel - tune.slowdown*bu  # for slower decay was 16.0 (19.5 for Germany)
				if desiredvel < tune.minvel:
					desiredvel = tune.minvel
				throttlesetting = self.pid.update( 1/60.0, deltapos, desiredvel )
				if ( deltapos < desiredvel ) :
					throttlesetting=1
				else :
					throttlesetting=0
				if abs(angl) > tune.maxangle:
					throttlesetting=-999
				if pidx in self.switching:
					sd = self.switching[pidx]
					if sd[0] == lap:
						switchidx = pidx
				print "abspos=%f angl=%f bu_l=%f bu_r=%f deltapos=%f desiredvel=%f throttle=%f" % ( abspos, angl, bu_l, bu_r, deltapos, desiredvel, throttlesetting )

		if switchidx >= 0:
			switchdata = self.switching[switchidx]
			direction = switchdata[1]
			self.switchlane(direction)
			switchdata[0] += 1
			self.switching[switchidx] = switchdata

		if throttlesetting < 0 :
			throttlesetting = 0
		if throttlesetting > 1 :
			throttlesetting = 1
		self.throttle( throttlesetting )

	def on_crash(self, data, gametick):
		print data
		print("Someone crashed")
		self.ping()

	def on_game_end(self, data, gametick):
		print data
		print("Race ended")
		self.ping()

	def on_error(self, data, gametick):
		print("Error: {0}".format(data))
		self.ping()

	def msg_loop(self):
		msg_map = {
			'join': self.on_join,
			'joinRace': self.on_join_race,
			'gameInit': self.on_game_init,
			'gameStart': self.on_game_start,
			'carPositions': self.on_car_positions,
			'crash': self.on_crash,
			'gameEnd': self.on_game_end,
			'error': self.on_error,
		}
		socket_file = s.makefile()
		line = socket_file.readline()
		while line:
			#print line
			msg = json.loads(line)
			msg_type, data = msg['msgType'], msg['data']
			gametick = self.tick
			if 'gameTick' in msg:
				gametick = msg['gameTick']
			if msg_type in msg_map:
				msg_map[msg_type](data,gametick)
			else:
				print("Got {0}".format(msg_type))
				self.ping()
			self.tick = gametick
			line = socket_file.readline()


if __name__ == "__main__":
	if len(sys.argv) != 5:
		print("Usage: ./run host port botname botkey")
	else:
		host, port, name, key = sys.argv[1:5]
		print("Connecting with parameters:")
		print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((host, int(port)))
		bot = NoobBot(s, name, key)
		bot.run()

